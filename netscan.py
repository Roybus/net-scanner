import socket, subprocess, sys, argparse
from datetime import datetime

#Clear the screen
subprocess.call('clear', shell=True)

#Arguments
parser = argparse.ArgumentParser(usage="Network Port Scanner", description="Example of use: python netscanner.py -H 192.168.0.1 -p 1-1000")
parser.add_argument('-H', '--host', help='IP of the remote host')
parser.add_argument('-p', '--ports', help='Port range to use')

args = parser.parse_args()
host = args.host
ports = args.ports.split('-')

startPort = int(ports[0])
endPort =  int(ports[1])

#Set the screen
print("-")*60
print("Please wait scanning host...")
print("-")*60

#Scanning process
startTime=datetime.now()
try:
    for port in range(startPort, endPort +1):
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        result = sock.connect_ex((host,port))
        if result == 0:
            print("Port {}:    Open".format(port))
            sock.close()

except KeyboardInterrupt:
    sys.exit()

except socket.gaierror:
    print("Hostname could not be resolved")
    sys.exit()

except socket.error:
    print("Could not connect to server")
    sys.exit()

finishTime = datetime.now()
totalTime = startTime - finishTime

print'Scanning completed in: ',totalTime